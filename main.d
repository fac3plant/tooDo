/*********************************************************************
* This is the beginning work on tooDo, a task application written in D.
* This project is more of a learning experience rather than anything actually useful.
*********************************************************************
* - TODO: Figure out structure of files
* - TODO: Figure out how to modify files
* - TODO: Figure out interface
* - TODO: Figure out keybindings
* - TODO: Figure out how to use $XDG for .config files
*/
import std.stdio;
import std.string;
import std.json;

char[] createFile(char[] input) {
  char[] newFile;
  newFile = strip(input);
  newFile ~= ".json";
  return newFile;
}

int createTempFile(File ogFile) {
  /* TODO: Copy filename of the original file */
  File tempFile = File("temp.json", "w+");
}

int main() {

  /* Proof of concept for custom filenames */
  char[] fileName;
  writeln("Please enter a file name: ");
  readln(fileName);
  fileName = createFile(fileName);

  File file = File(fileName, "w+");

  /* Proof of concept for actually parsing JSON */
  File jsonFile = File("test.json", "r");
  string s = strip(jsonFile.readln());
  JSONValue j = parseJSON(s);

  writeln(j["language"].str); // Should return "D"
  writeln(j["rating"].floating); // Should return 3.5

  //file.close;

  return 0;
}
